package komodo.documentstore.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "document", schema = "documentstore")
@NoArgsConstructor
@Getter
@Setter
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "document_id")
    private int id;

    @Basic
    @Column(name = "name", nullable = false)
    private String name;

    @Basic
    @Column(name = "extension", nullable = false)
    private String extension;

    @Basic
    @Column(name = "description", nullable = false)
    private String description;

    @Basic
    @Column(name = "upload_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadDate = new Date();

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "owner")
    private User owner;

    @Basic
    @Column(name = "filename", nullable = false)
    private String filename;

    @Basic
    @Column(name = "view_anyone", nullable = false)
    private DocumentAccessLevel viewLevel = DocumentAccessLevel.NOBODY;

    @Basic
    @Column(name = "edit_anyone", nullable = false)
    private DocumentAccessLevel editLevel = DocumentAccessLevel.NOBODY;

    @Basic
    @Column(name = "delete_anyone", nullable = false)
    private DocumentAccessLevel deleteLevel = DocumentAccessLevel.NOBODY;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "user_document_access_view",
            inverseJoinColumns = @JoinColumn(name = "user_id"),
            joinColumns = @JoinColumn(name = "document_id"))
    private Set<User> viewUsersList = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "user_document_access_edit",
            inverseJoinColumns = @JoinColumn(name = "user_id"),
            joinColumns = @JoinColumn(name = "document_id"))
    private Set<User> editUsersList = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "user_document_access_delete",
            inverseJoinColumns = @JoinColumn(name = "user_id"),
            joinColumns = @JoinColumn(name = "document_id"))
    private Set<User> deleteUsersList = new HashSet<>();

    public void clearAssociations() {

        getOwner().getOwnedDocuments().remove(this);
        setOwner(null);
        getViewUsersList().forEach(user -> user.getDocumentAccessView().remove(this));
        getViewUsersList().clear();
        getEditUsersList().forEach(user -> user.getDocumentAccessEdit().remove(this));
        getEditUsersList().clear();
        getDeleteUsersList().forEach(user -> user.getDocumentAccessDelete().remove(this));
        getDeleteUsersList().clear();
    }

    @Override
    public boolean equals(Object other) {
        if (this.getClass().isInstance(other)) {
            return this.getId() == ((Document) other).getId();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.getId();
    }
}
