package komodo.documentstore.model.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public enum DocumentAccessLevel {

    NOBODY(1),
    SOMEBODY(2),
    ANYBODY(3);

    @Getter
    @Setter
    private int value;

    DocumentAccessLevel(int value) {
        this.value = value;
    }

    public List<String> getAsList() {
        List<String> list = new ArrayList<>();
        DocumentAccessLevel[] documentAccessLevels = DocumentAccessLevel.values();
        for (DocumentAccessLevel dal : documentAccessLevels) {
            list.add(dal.getValue(), dal.name());
        }
        return list;
    }

}
