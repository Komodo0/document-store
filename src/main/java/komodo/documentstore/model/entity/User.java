package komodo.documentstore.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


@Entity
@Table(name = "user", schema = "documentstore", uniqueConstraints = @UniqueConstraint(columnNames = {"username"}))
@NoArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Basic
    @Column(name = "username", nullable = false)
    private String username;

    @Basic
    @Column(name = "password", nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_to_user_role",
            joinColumns = @JoinColumn(name = "user_id", nullable = false, updatable = false),
            inverseJoinColumns = @JoinColumn(name = "role_id", nullable = false, updatable = false))
    private Set<UserRole> userRoles = new HashSet<>(0);

    @Basic
    @Column(name = "registration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate = new Date();

    @Basic
    @Column(name = "confirmed", nullable = false, columnDefinition = "BOOLEAN")
    private Boolean confirmed = false;

    @Basic
    @Column(name = "confirm_token")
    private String confirmToken = UUID.randomUUID().toString();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, mappedBy = "owner")
    private Set<Document> ownedDocuments = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "viewUsersList", cascade = CascadeType.MERGE)
    private Set<Document> documentAccessView = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "editUsersList", cascade = CascadeType.MERGE)
    private Set<Document> documentAccessEdit = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "deleteUsersList", cascade = CascadeType.MERGE)
    private Set<Document> documentAccessDelete = new HashSet<>();

    public void addUserRole(UserRole userRole) {
        getUserRoles().add(userRole);
    }

    @Override
    public boolean equals(Object other) {
        if (this.getClass().isInstance(other)) {
            return this.getId() == ((User) other).getId();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.getId();
    }


}
