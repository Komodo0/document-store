package komodo.documentstore.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user_role", schema = "documentstore", uniqueConstraints = @UniqueConstraint(columnNames = {"role"}))
@NoArgsConstructor
@Getter
@Setter
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id", unique = true, nullable = false)
    private int id;

    @Basic
    @Column(name = "role", nullable = false, length = 45)
    private String role;

    //    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    //    @JoinTable(name = "user_to_user_role",
    //            joinColumns = @JoinColumn(name = "role_id", nullable = false, updatable = false),
    //            inverseJoinColumns = @JoinColumn(name = "user_id", nullable = false, updatable = false))
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "userRoles", cascade = CascadeType.ALL)
    private Set<User> users = new HashSet<>(0);

    @Override
    public boolean equals(Object other) {
        if (this.getClass().isInstance(other)) {
            return this.getId() == ((UserRole) other).getId();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.getId();
    }

}
