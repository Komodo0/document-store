package komodo.documentstore.model.form;

import komodo.documentstore.model.entity.DocumentAccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class DocumentCreationForm {

    private String name;

    private String description;

    private MultipartFile file;

    private DocumentAccessLevel viewAccessLevel;

    private DocumentAccessLevel editAccessLevel;

    private DocumentAccessLevel deleteAccessLevel;

    private Set<String> viewAccessUserIds;

    private Set<String> editAccessUserIds;

    private Set<String> deleteAccessUserIds;

}
