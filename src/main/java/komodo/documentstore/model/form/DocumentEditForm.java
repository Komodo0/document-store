package komodo.documentstore.model.form;

import komodo.documentstore.model.entity.Document;
import komodo.documentstore.model.entity.DocumentAccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class DocumentEditForm {

    private String name;

    private String description;

    private DocumentAccessLevel viewAccessLevel;

    private DocumentAccessLevel editAccessLevel;

    private DocumentAccessLevel deleteAccessLevel;

    private Set<String> viewAccessUserIds;

    private Set<String> editAccessUserIds;

    private Set<String> deleteAccessUserIds;

    public DocumentEditForm(Document document) {
        setName(document.getName());
        setDescription(document.getDescription());
        setViewAccessLevel(document.getViewLevel());
        setEditAccessLevel(document.getEditLevel());
        setDeleteAccessLevel(document.getDeleteLevel());
        setEditAccessUserIds(document.getEditUsersList().stream().map(u -> String.valueOf(u.getId())).collect(Collectors.toSet()));
        setViewAccessUserIds(document.getViewUsersList().stream().map(u -> String.valueOf(u.getId())).collect(Collectors.toSet()));
        setDeleteAccessUserIds(document.getDeleteUsersList().stream().map(u -> String.valueOf(u.getId())).collect(Collectors.toSet()));
    }

}
