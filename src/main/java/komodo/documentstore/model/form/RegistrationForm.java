package komodo.documentstore.model.form;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RegistrationForm {

    private String username;

    private String password;

    private String confirmPassword;

}
