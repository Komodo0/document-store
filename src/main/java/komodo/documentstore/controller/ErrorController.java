package komodo.documentstore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

@Controller
public class ErrorController {

    String getErrorPage(int errorCode, String errorMessage, ModelMap modelMap) {
        modelMap.put("errorCode", errorCode);
        modelMap.put("errorText", errorMessage);
        return "error";
    }

}
