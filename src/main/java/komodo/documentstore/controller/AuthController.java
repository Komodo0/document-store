package komodo.documentstore.controller;

import komodo.documentstore.controller.validator.RegistrationValidator;
import komodo.documentstore.model.entity.User;
import komodo.documentstore.model.form.RegistrationForm;
import komodo.documentstore.service.MailSenderService;
import komodo.documentstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuthController {

    @Autowired
    private RegistrationValidator registrationValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private MailSenderService mailSenderService;

    @Autowired
    @Qualifier("serverUri")
    private String serverUri;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView modelAndView = new ModelAndView();
        if (error != null) {
            modelAndView.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            modelAndView.addObject("msg", "You've been logged out successfully.");
        }

        modelAndView.setViewName("login");

        return modelAndView;
    }

    @RequestMapping(value = "/init", method = RequestMethod.GET)
    public String register(ModelMap model) {
        RegistrationForm registerForm = new RegistrationForm();
        model.put("registrationForm", registerForm);
        return "registration";
    }

    @RequestMapping(value = "/init", method = RequestMethod.POST)
    public String processRegister(RegistrationForm registerForm, BindingResult result, ModelMap model) {
        registrationValidator.validate(registerForm, result);
        if (result.hasErrors()) {
            model.put("registrationForm", registerForm);
            return "registration";
        }
        userService.registerNewUser(registerForm.getUsername(), registerForm.getPassword());
        User user = userService.getUserByUsername(registerForm.getUsername());
        String messageText = "Activate your account using this link: " +
                serverUri + "account-confirm?username=" + user.getUsername() + "&token=" + user.getConfirmToken();
        mailSenderService.addMessageTask(user.getUsername(), "Account validation", messageText, 3);
        return "registration-success";
    }

    @RequestMapping(value = "/account-confirm", method = RequestMethod.GET)
    public ModelAndView confirm(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "token") String token) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getUserByUsername(username);
        if (user.getConfirmed() || !user.getConfirmToken().equals(token)) {
            modelAndView.setViewName("error");
            modelAndView.addObject("errorCode", "404");
            modelAndView.addObject("errorText", "It's nothing here!");
        } else {
            user.setConfirmed(true);
            userService.edit(user);
            modelAndView.setViewName("confirmation-success");
        }
        return modelAndView;
    }


}
