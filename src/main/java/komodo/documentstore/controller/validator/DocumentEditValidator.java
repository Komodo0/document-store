package komodo.documentstore.controller.validator;

import komodo.documentstore.model.form.DocumentCreationForm;
import komodo.documentstore.model.form.DocumentEditForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Set;

@Component
public class DocumentEditValidator implements Validator {

    @Autowired
    @Qualifier("documentAcceptedTypes")
    private Set<String> documentAcceptedTypes;

    @Override
    public boolean supports(Class<?> clazz) {
        return DocumentCreationForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        DocumentEditForm documentEditForm = (DocumentEditForm) target;
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty", "Document name must not be empty.");
    }

}
