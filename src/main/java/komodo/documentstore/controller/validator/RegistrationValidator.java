package komodo.documentstore.controller.validator;

import komodo.documentstore.model.form.RegistrationForm;
import komodo.documentstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RegistrationValidator implements Validator {

    @Autowired
    private UserService userService;

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Override
    public boolean supports(Class<?> clazz) {
        return RegistrationForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrationForm registerForm = (RegistrationForm) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "username.empty", "Username must not be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password.empty", "Password must not be empty.");

        if (userService.userExists(registerForm.getUsername())) {
            errors.rejectValue("username", "username.alreadyExists", "This username already exists!");
        }

        String username = registerForm.getUsername();
        if ((username.length()) > 70) {
            errors.rejectValue("username", "username.tooLong", "Username must not more than 70 characters.");
        }
        if ((username.length()) < 7) {
            errors.rejectValue("username", "username.tooShort", "Username must not less than 7 characters.");
        }

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(username);
        if (!matcher.matches()) {
            errors.rejectValue("username", "username.invalid", "Username must be a valid email address!");
        }

        if (registerForm.getPassword().length() < 5) {
            errors.rejectValue("confirmPassword", "password.tooShort", "Passwords must be more than 5 characters.");
        }

        if (!(registerForm.getPassword()).equals(registerForm.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "confirmPassword.passwordDontMatch", "Passwords don't match.");
        }



    }

}
