package komodo.documentstore.controller.validator;

import komodo.documentstore.model.form.DocumentCreationForm;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

@Component
public class DocumentCreationValidator implements Validator {

    @Autowired
    @Qualifier("documentAcceptedTypes")
    private Set<String> documentAcceptedTypes;

    @Override
    public boolean supports(Class<?> clazz) {
        return DocumentCreationForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        DocumentCreationForm documentCreationForm = (DocumentCreationForm) target;

        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty", "Document name must not be empty.");
        MultipartFile file = documentCreationForm.getFile();

        if (file.isEmpty()) {
            errors.rejectValue("file", "file.empty", "Uploaded file must not be empty.");
        }

        if (!documentAcceptedTypes.contains(FilenameUtils.getExtension(file.getOriginalFilename()))) {
            errors.rejectValue("file", "file.type", "Wrong file type! Accepted types: " + String.join(", ", documentAcceptedTypes));
        }

    }
}
