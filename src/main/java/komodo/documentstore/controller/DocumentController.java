package komodo.documentstore.controller;

import komodo.documentstore.controller.validator.DocumentCreationValidator;
import komodo.documentstore.controller.validator.DocumentEditValidator;
import komodo.documentstore.model.entity.Document;
import komodo.documentstore.model.entity.DocumentAccessLevel;
import komodo.documentstore.model.entity.User;
import komodo.documentstore.model.form.DocumentCreationForm;
import komodo.documentstore.model.form.DocumentEditForm;
import komodo.documentstore.service.DocumentService;
import komodo.documentstore.service.UserService;
import komodo.documentstore.utils.HtmlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/doc")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private DocumentCreationValidator documentCreationValidator;

    @Autowired
    private DocumentEditValidator documentEditValidator;

    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("documentDirectory")
    private String documentDirectory;

    @Autowired
    private ErrorController errorController;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String documentsList(ModelMap model) {
        //TODO: Выводит список документов, в зависимости от параметров запроса - фильтрация(поиск) и сортировка.
        if (getCurrentUser() == null) {
            return errorController.getErrorPage(401, "Access denied", model);
        }
        model.put("documents", documentService.getAvailableDocuments(getCurrentUser()));
        return "documents-list";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addDocumentPage(ModelMap model) {
        DocumentCreationForm documentCreationForm = new DocumentCreationForm();
        model.put("documentCreationForm", documentCreationForm);
        model.put("accessLevels", Arrays.asList(DocumentAccessLevel.values()));
        model.put("usersList", userService.getAllUsers());
        return "add-document";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addDocumentProcess(DocumentCreationForm documentCreationForm, BindingResult result, ModelMap model) throws UnsupportedEncodingException {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return errorController.getErrorPage(401, "Access denied", model);
        }
        documentCreationValidator.validate(documentCreationForm, result);
        if (result.hasErrors()) {
            model.put("documentCreationForm", documentCreationForm);
            model.put("accessLevels", Arrays.asList(DocumentAccessLevel.values()));
            model.put("usersList", userService.getAllUsers());
            return "add-document";
        }

        Document document = documentService.addNewDocument(documentCreationForm.getName(),
                new String(documentCreationForm.getDescription().getBytes(Charset.forName("UTF-8")), Charset.forName("UTF-8")),
                documentCreationForm.getFile(),
                currentUser,
                documentCreationForm.getViewAccessLevel(),
                documentCreationForm.getEditAccessLevel(),
                documentCreationForm.getDeleteAccessLevel(),
                (documentCreationForm.getViewAccessUserIds() == null) ? new HashSet<>() : documentCreationForm.getViewAccessUserIds(),
                (documentCreationForm.getEditAccessUserIds() == null) ? new HashSet<>() : documentCreationForm.getEditAccessUserIds(),
                (documentCreationForm.getDeleteAccessUserIds() == null) ? new HashSet<>() : documentCreationForm.getEditAccessUserIds());

        if (document == null) {
            model.put("documentCreationForm", documentCreationForm);
            return "add-document";
        }
        return "redirect:/doc/view/" + document.getId();
    }

    @RequestMapping(value = "/view/{fileId}", method = RequestMethod.GET)
    public String viewDocument(ModelMap model,
                               @PathVariable("fileId") String documentId) {
        try {
            int docId = Integer.parseInt(documentId);
            Document document = documentService.getDocument(docId);
            if (document == null) {
                return errorController.getErrorPage(404, "Page does not exist.", model);
            }
            if (!documentService.viewDocumentAccepted(document, getCurrentUser())) {
                return errorController.getErrorPage(401, "Access denied", model);
            }

            if (documentService.editDocumentAccepted(document, getCurrentUser())) {
                model.put("editAccepted", true);
                if (documentService.deleteDocumentAccepted(document, getCurrentUser())) {
                    model.put("deleteAccepted", true);
                }
            }
            model.put("document", document);
            return "view-document";
        } catch (NumberFormatException | NullPointerException e) {
            return errorController.getErrorPage(404, "Page does not exist.", model);
        }
    }

    @RequestMapping(value = "/download/{fileId}")
    public void downloadDocumentFile(HttpServletRequest request,
                                     HttpServletResponse response,
                                     @PathVariable("fileId") String documentId) {
        Document document = documentService.getDocument(Integer.parseInt(documentId));
        Path file = Paths.get(documentDirectory, document.getFilename());
        String fileName = document.getName() + "." + document.getExtension();
        if (Files.exists(file)) {
            response.setContentType(HtmlUtils.getFileContentType(document.getExtension()));
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            try {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @RequestMapping(value = "/edit/{fileId}", method = RequestMethod.GET)
    public String editDocumentPage(@PathVariable("fileId") String documentId, ModelMap model) {
        try {
            int docId = Integer.parseInt(documentId);
            Document document = documentService.getDocument(docId);
            if (document == null) {
                return errorController.getErrorPage(404, "Page does not exist.", model);
            }
            if (!documentService.editDocumentAccepted(document, getCurrentUser())) {
                return errorController.getErrorPage(401, "Access denied", model);
            }
            model.put("documentEditForm", new DocumentEditForm(document));
            model.put("documentId", document.getId());
            model.put("accessLevels", Arrays.asList(DocumentAccessLevel.values()));
            model.put("usersList", userService.getAllUsers());
            return "edit-document";
        } catch (NumberFormatException | NullPointerException e) {
            return errorController.getErrorPage(404, "Page does not exist.", model);
        }
    }

    @RequestMapping(value = "/edit/{fileId}", method = RequestMethod.POST)
    public String editDocumentProcess(@PathVariable("fileId") String documentId,
                                      DocumentEditForm documentEditForm,
                                      BindingResult result,
                                      ModelMap model) {

        try {
            int docId = Integer.parseInt(documentId);
            Document document = documentService.getDocument(docId);
            if (document == null) {
                return errorController.getErrorPage(404, "Page does not exist.", model);
            }
            if (!documentService.editDocumentAccepted(document, getCurrentUser())) {
                return errorController.getErrorPage(401, "Access denied", model);
            }

            documentEditValidator.validate(documentEditForm, result);
            if (result.hasErrors()) {
                model.put("documentEditForm", documentEditForm);
                model.put("accessLevels", Arrays.asList(DocumentAccessLevel.values()));
                model.put("usersList", userService.getAllUsers());
                return "add-document";
            }

            document.setName(documentEditForm.getName());
            document.setDescription(documentEditForm.getDescription());
            document.setViewLevel(documentEditForm.getViewAccessLevel());
            document.setEditLevel(documentEditForm.getEditAccessLevel());
            document.setDeleteLevel(documentEditForm.getDeleteAccessLevel());
            document.setViewUsersList(documentEditForm.getViewAccessUserIds() == null ? new HashSet<>() : documentEditForm.getViewAccessUserIds().stream().map(userId -> userService.getCachedOrFromDb(Integer.parseInt(userId))).collect(Collectors.toSet()));
            document.setEditUsersList(documentEditForm.getEditAccessUserIds() == null ? new HashSet<>() : documentEditForm.getEditAccessUserIds().stream().map(userId -> userService.getCachedOrFromDb(Integer.parseInt(userId))).collect(Collectors.toSet()));
            document.setDeleteUsersList(documentEditForm.getDeleteAccessUserIds() == null ? new HashSet<>() : documentEditForm.getDeleteAccessUserIds().stream().map(userId -> userService.getCachedOrFromDb(Integer.parseInt(userId))).collect(Collectors.toSet()));
            documentService.edit(document);
            System.out.println(document);
            return "redirect:/doc/view/" + document.getId();

        } catch (NumberFormatException | NullPointerException e) {
            return errorController.getErrorPage(404, "Page does not exist.", model);
        }
    }

    @RequestMapping(value = "/delete/{fileId}", method = RequestMethod.GET)
    public String deleteDocumentPage(ModelMap model, @PathVariable("fileId") String documentId) {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return errorController.getErrorPage(401, "Access denied", model);
        }
        try {
            int docId = Integer.parseInt(documentId);
            Document document = documentService.getDocument(docId);
            if (document == null) {
                return errorController.getErrorPage(404, "Page does not exist.", model);
            }
            if (!documentService.deleteDocumentAccepted(document, getCurrentUser())) {
                return errorController.getErrorPage(401, "Access denied", model);
            }
            model.put("document", document);
            documentService.delete(Integer.parseInt(documentId));
            return "delete-document-success";
        } catch (NumberFormatException | NullPointerException e) {
            return errorController.getErrorPage(404, "Page does not exist.", model);
        }
    }

    private User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return null;
        }
        User user = userService.getUserByUsername(auth.getName());
        return userService.getCachedOrFromDb(user.getId());
    }

}
