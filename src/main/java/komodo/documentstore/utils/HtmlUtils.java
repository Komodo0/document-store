package komodo.documentstore.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HtmlUtils {

    public static String getDescriptionForHtml(String str) {
        return ws2code(nl2br(str));
    }

    private static String nl2br(String input) {
        Scanner scanner = new Scanner(input);
        List<String> lines = new ArrayList<>();
        do {
            lines.add(scanner.nextLine());
        } while (scanner.hasNextLine());

        return String.join("<br/>", lines);
    }

    private static String ws2code(String str) {
        return str.replace("\\s", "&nbsp;");
    }

    public static String getFileContentType(String extension) {
        switch (extension) {
            case "doc":
                return "application/msword";
            case "docx":
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case "xls":
                return "application/vnd.ms-excel";
            case "xlsx":
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            case "pdf":
                return "application/pdf";
            default:
                return null;
        }
    }

}
