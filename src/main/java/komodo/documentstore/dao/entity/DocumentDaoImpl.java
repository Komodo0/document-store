package komodo.documentstore.dao.entity;

import komodo.documentstore.model.entity.Document;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DocumentDaoImpl implements DocumentDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(Document document) {
        session.getCurrentSession().save(document);
    }

    @Override
    public void edit(Document document) {
        session.getCurrentSession().update(document);
    }

    @Override
    public void delete(int documentId) {
        session.getCurrentSession().delete(getDocument(documentId));
    }

    @Override
    public Document getDocument(int documentId) {
        return (Document) session.getCurrentSession().get(Document.class, documentId);
    }

    @Override
    public List getAllDocuments() {
        return session.getCurrentSession().createQuery("from Document").list();
    }

}
