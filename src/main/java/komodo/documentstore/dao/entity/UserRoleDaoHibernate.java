package komodo.documentstore.dao.entity;

import komodo.documentstore.model.entity.UserRole;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class UserRoleDaoHibernate implements UserRoleDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(UserRole userRole) {
        session.getCurrentSession().save(userRole);
    }

    @Override
    public void edit(UserRole userRole) {
        session.getCurrentSession().update(userRole);
    }

    @Override
    public void delete(int userRoleId) {
        session.getCurrentSession().delete(getUserRole(userRoleId));
    }

    @Override
    public UserRole getUserRole(int userRoleId) {
        return (UserRole) session.getCurrentSession().get(UserRole.class, userRoleId);
    }

    @Override
    public UserRole getUserRoleByName(String userRoleName) {
        try {
            return (UserRole) session.getCurrentSession().createQuery("select u from UserRole u where u.role = :userRoleName").setParameter("userRoleName", userRoleName).setCacheable(true).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List getAllUserRoles() {
        return session.getCurrentSession().createQuery("from UserRole").list();
    }

}
