package komodo.documentstore.dao.entity;

import komodo.documentstore.model.entity.Document;

import java.util.List;

public interface DocumentDao {

    void add(Document document);

    void edit(Document document);

    void delete(int documentId);

    Document getDocument(int documentId);

    List getAllDocuments();

}
