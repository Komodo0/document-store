package komodo.documentstore.dao.entity;

import komodo.documentstore.model.entity.UserRole;

import java.util.List;

public interface UserRoleDao {

    void add(UserRole userRole);

    void edit(UserRole userRole);

    void delete(int userRoleId);

    UserRole getUserRole(int userRoleId);

    UserRole getUserRoleByName(String userRoleName);

    List getAllUserRoles();

}
