package komodo.documentstore.dao.entity;

import komodo.documentstore.model.entity.User;

import java.util.List;

public interface UserDao {

    void add(User user);

    void edit(User user);

    void delete(int userId);

    User getUser(int userId);

    User getUserByUsername(String username);

    List getAllUsers();

}
