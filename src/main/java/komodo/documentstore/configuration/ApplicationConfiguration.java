package komodo.documentstore.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

@Configuration
@PropertySource({"classpath:application.properties"})
@ComponentScan(basePackages = "komodo.documentstore")
public class ApplicationConfiguration {

    @Autowired
    private Environment env;

    @Bean(name = "documentDirectory")
    public String getDocumentsDirectory() {
        return env.getProperty("document.directory");
    }

    @Bean(name = "documentAcceptedTypes")
    public Set<String> getDocumentAcceptedTypes() {
        return new HashSet<>(Arrays.asList(env.getProperty("document.accepted.types", String[].class)));
    }

    @Bean(name = "serverUri")
    public String getServerUri() {
        return env.getProperty("server.uri");
    }

    @Bean
    public JavaMailSender getMailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(env.getProperty("spring.mail.host"));
        javaMailSender.setPort(Integer.parseInt(env.getProperty("spring.mail.port")));
        javaMailSender.setUsername(env.getProperty("spring.mail.username"));
        javaMailSender.setPassword(env.getProperty("spring.mail.password"));

        Properties javaMailProperties = new Properties();
        javaMailProperties.setProperty("mail.transport.protocol", env.getProperty("spring.mail.transport.protocol"));
        javaMailProperties.setProperty("mail.smtp.auth", env.getProperty("spring.mail.smtp.auth"));
        javaMailProperties.setProperty("mail.smtp.starttls.enable", env.getProperty("spring.mail.smtp.starttls.enable"));
        javaMailProperties.setProperty("mail.debug", env.getProperty("spring.mail.debug"));
        javaMailProperties.setProperty("mail.smtps.ssl.checkserveridentity", env.getProperty("spring.mail.smtps.ssl.checkserveridentity"));
        javaMailProperties.setProperty("mail.smtps.ssl.trust", env.getProperty("spring.mail.smtps.ssl.trust"));

        javaMailSender.setJavaMailProperties(javaMailProperties);

        return javaMailSender;
    }

    @Bean
    public SimpleMailMessage getMailMessage() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(env.getProperty("spring.mail.sender"));
        return message;
    }

}
