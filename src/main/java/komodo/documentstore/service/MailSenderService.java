package komodo.documentstore.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@EnableAsync
@EnableScheduling
@Service
public class MailSenderService {

    @Autowired
    private MailSender mailSender;

    @Autowired
    private SimpleMailMessage mailMessage;

    private Set<MessageTask> messageTasks = new HashSet<>();

    @Scheduled(fixedDelay = 2 * 60 * 1000)
    protected void trySendMessages() {
        System.out.println("SCHEDULED MESSAGE SENDING IS STARTED!!!!");
        this.messageTasks.forEach(this::sendMessageTask);
    }

    @Async
    void sendMessageTask(MessageTask messageTask) {
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("SENDING: " + messageTask.getMessageTo());
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            mailMessage.copyTo(message);
            message.setSubject(messageTask.getSubject());
            message.setTo(messageTask.getMessageTo());
            message.setText(messageTask.getMessageText());
            mailSender.send(message);
            messageTasks.remove(messageTask);
        } catch (Exception e) {
            e.printStackTrace();
            if (messageTask.getAttempts() <= messageTask.getAttemptsMade() + 1) {
                this.messageTasks.remove(messageTask);
            } else {
                messageTask.setAttemptsMade(messageTask.getAttemptsMade() + 1);
            }
        }

    }

    public void addMessageTask(String messageTo, String subject, String messageText, int attempts) {
        messageTasks.add(new MessageTask(messageTo, subject, messageText, attempts));
    }

    @Getter
    @Setter
    class MessageTask {

        private String messageTo;
        private String subject;
        private String messageText;
        private int attempts;

        private int attemptsMade = 0;

        MessageTask(String messageTo, String subject, String messageText, int attempts) {
            this.messageTo = messageTo;
            this.subject = subject;
            this.messageText = messageText;
            this.attempts = attempts;
        }

    }

}
