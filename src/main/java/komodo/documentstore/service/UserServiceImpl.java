package komodo.documentstore.service;

import komodo.documentstore.dao.entity.UserDao;
import komodo.documentstore.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleService userRoleService;

    private Map<Integer, User> usersCache = new HashMap<>();

    @Transactional
    public void add(User user) {
        userDao.add(user);
    }

    @Transactional
    public void edit(User user) {
        userDao.edit(user);
    }

    @Transactional
    public void delete(int userId) {
        userDao.delete(userId);
    }

    @Transactional
    public User getUser(int userId) {
        return userDao.getUser(userId);
    }

    @Transactional
    public User getCachedOrFromDb(int userId) {
        if (!this.usersCache.containsKey(userId)) {
            this.usersCache.put(userId, getUser(userId));
        }
        return this.usersCache.get(userId);
    }

    @Transactional
    public User getUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

    @Transactional
    public List getAllUsers() {
        return userDao.getAllUsers();
    }

    @Transactional
    public boolean registerNewUser(String username, String password) {
        User user = getUserByUsername(username);
        if (user == null) {
            user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.addUserRole(userRoleService.getUserRoleByName("USER"));
            add(user);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean userExists(String username) {
        return (getUserByUsername(username) != null);
    }

    @Override
    public User getUserOrNullByStringId(String userId) {
        try {
            int usrId = Integer.parseInt(userId);
            return getCachedOrFromDb(usrId);
        } catch (NullPointerException | NumberFormatException e) {
            e.printStackTrace();
            return null;
        }
    }

}
