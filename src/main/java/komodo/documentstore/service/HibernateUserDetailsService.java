package komodo.documentstore.service;

import komodo.documentstore.model.entity.User;
import komodo.documentstore.model.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("HibernateUserDetailsService")
public class HibernateUserDetailsService implements UserDetailsService {


    @Autowired
    private UserService userService;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUserByUsername(username);
        System.out.println("Search for user with username: " + username);
        if (user == null || !user.getConfirmed()) {
            System.out.println("User not found!");
            throw new UsernameNotFoundException("User not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                true, true, true, true, getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        for (UserRole userRole : user.getUserRoles()) {
            System.out.println("UserProfile : " + userRole);
            authorities.add(new SimpleGrantedAuthority("ROLE_" + userRole.getRole()));
        }
        System.out.print("authorities :" + authorities);
        return authorities;
    }

}
