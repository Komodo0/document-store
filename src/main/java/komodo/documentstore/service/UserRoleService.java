package komodo.documentstore.service;

import komodo.documentstore.model.entity.UserRole;

import java.util.List;

public interface UserRoleService {

    void add(UserRole userRole);

    void edit(UserRole userRole);

    void delete(int userRoleId);

    UserRole getUserRole(int userRoleId);

    UserRole getUserRoleByName(String roleName);

    List getAllUserRoles();
}
