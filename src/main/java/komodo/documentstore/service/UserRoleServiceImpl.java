package komodo.documentstore.service;

import komodo.documentstore.dao.entity.UserRoleDao;
import komodo.documentstore.model.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleDao userRoleDao;

    @Transactional
    public void add(UserRole userRole) {
        userRoleDao.add(userRole);
    }

    @Transactional
    public void edit(UserRole userRole) {
        userRoleDao.edit(userRole);
    }

    @Transactional
    public void delete(int userRoleId) {
        userRoleDao.delete(userRoleId);
    }

    @Transactional
    public UserRole getUserRole(int userRoleId) {
        return userRoleDao.getUserRole(userRoleId);
    }

    @Transactional
    public UserRole getUserRoleByName(String roleName) {
        return userRoleDao.getUserRoleByName(roleName);
    }

    @Transactional
    public List getAllUserRoles() {
        return userRoleDao.getAllUserRoles();
    }

}
