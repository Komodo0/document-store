package komodo.documentstore.service;

import komodo.documentstore.model.entity.User;

import java.util.List;

public interface UserService {

    void add(User user);

    void edit(User user);

    void delete(int userId);

    User getUser(int userId);

    User getCachedOrFromDb(int userId);

    User getUserByUsername(String username);

    List getAllUsers();

    boolean registerNewUser(String username, String password);

    boolean userExists(String username);

    User getUserOrNullByStringId(String userId);

}
