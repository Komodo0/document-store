package komodo.documentstore.service;

import komodo.documentstore.model.entity.Document;
import komodo.documentstore.model.entity.DocumentAccessLevel;
import komodo.documentstore.model.entity.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface DocumentService {

    void add(Document document);

    void edit(Document document);

    void delete(int documentId);

    Document getDocument(int documentId);

    Document getCachedOrFromDb(int documentId);

    List getAllDocuments();

    Document addNewDocument(String name,
                            String description,
                            MultipartFile file,
                            User owner,
                            DocumentAccessLevel viewAccessLevel,
                            DocumentAccessLevel editAccessLevel,
                            DocumentAccessLevel deleteAccessLevel,
                            Set<String> viewAccessUsersIds,
                            Set<String> editAccessUsersIds,
                            Set<String> deleteAccessUsersIds);

    List<Document> getAvailableDocuments(User user);

    boolean viewDocumentAccepted(Document document, User user);

    boolean editDocumentAccepted(Document document, User user);

    boolean deleteDocumentAccepted(Document document, User user);
}
