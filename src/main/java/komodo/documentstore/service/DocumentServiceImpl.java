package komodo.documentstore.service;

import komodo.documentstore.dao.entity.DocumentDao;
import komodo.documentstore.model.entity.Document;
import komodo.documentstore.model.entity.DocumentAccessLevel;
import komodo.documentstore.model.entity.User;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    DocumentDao documentDao;

    @Autowired
    @Qualifier("documentDirectory")
    private String documentDirectory;

    @Autowired
    UserService userService;

    private Map<Integer, Document> documentsCache = new HashMap<>();

    @Transactional
    public void add(Document document) {
        documentDao.add(document);
    }

    @Transactional
    public void edit(Document document) {
        documentDao.edit(document);
    }

    @Transactional
    public void delete(int documentId) {
        Document document = getDocument(documentId);
        if (document != null) {
            document.clearAssociations();
            edit(document);
            deleteDocumentFile(document);
            documentDao.delete(documentId);
        }
    }

    @Transactional
    public Document getDocument(int documentId) {
        return documentDao.getDocument(documentId);
    }

    @Override
    public Document getCachedOrFromDb(int documentId) {
        if (!this.documentsCache.containsKey(documentId)) {
            this.documentsCache.put(documentId, getDocument(documentId));
        }
        return this.documentsCache.get(documentId);
    }

    @Transactional
    public List getAllDocuments() {
        return documentDao.getAllDocuments();
    }

    @Transactional
    public Document addNewDocument(String name,
                                   String description,
                                   MultipartFile file,
                                   User owner,
                                   DocumentAccessLevel viewAccessLevel,
                                   DocumentAccessLevel editAccessLevel,
                                   DocumentAccessLevel deleteAccessLevel,
                                   Set<String> viewAccessUsersIds,
                                   Set<String> editAccessUsersIds,
                                   Set<String> deleteAccessUsersIds) {
        Document document = new Document();
        String fileName = saveFile(file);
        if (fileName == null) return null;
        document.setName(name);
        document.setExtension(FilenameUtils.getExtension(file.getOriginalFilename()));
        document.setFilename(fileName);
        document.setDescription(description);
        document.setOwner(owner);
        owner.getOwnedDocuments().add(document);
        document.setViewLevel(viewAccessLevel);
        document.setEditLevel(editAccessLevel);
        document.setDeleteLevel(deleteAccessLevel);

        Set<User> viewAccessUsers = viewAccessUsersIds.stream().
                map(userId -> {
                    User user = userService.getUserOrNullByStringId(userId);
                    user.getDocumentAccessView().add(document);
                    return user;
                }).
                filter(Objects::nonNull).
                collect(Collectors.toSet());
        document.setViewUsersList(viewAccessUsers);

        Set<User> editAccessUsers = editAccessUsersIds.stream().
                map(userId -> {
                    User user = userService.getUserOrNullByStringId(userId);
                    user.getDocumentAccessEdit().add(document);
                    return user;
                }).
                filter(Objects::nonNull).
                collect(Collectors.toSet());
        document.setEditUsersList(editAccessUsers);

        Set<User> deleteAccessUsers = deleteAccessUsersIds.stream().
                map(userId -> {
                    User user = userService.getUserOrNullByStringId(userId);
                    user.getDocumentAccessDelete().add(document);
                    return user;
                }).
                filter(Objects::nonNull).
                collect(Collectors.toSet());
        document.setDeleteUsersList(deleteAccessUsers);
        documentDao.add(document);
        return document;
    }


    @Transactional
    public List<Document> getAvailableDocuments(User user) {
        Set<Document> documents = new HashSet<>();
        documents.addAll(user.getOwnedDocuments());
        documents.addAll(user.getDocumentAccessView());
        documents.addAll(user.getDocumentAccessEdit());
        documents.addAll(user.getDocumentAccessDelete());
        return new ArrayList<>(documents);
    }

    @Transactional
    public String saveFile(MultipartFile file) {
        try {
            String fileName = UUID.randomUUID().toString();
            byte[] bytes = file.getBytes();
            if (!Files.exists(Paths.get(documentDirectory))) {
                Files.createDirectory(Paths.get(documentDirectory));
            }
            Files.write(Paths.get(documentDirectory + fileName), bytes);
            return fileName;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Transactional
    public void deleteDocumentFile(Document document) {
        if (document != null) {
            Path file = Paths.get(documentDirectory, document.getFilename());
            if (Files.exists(file)) {
                try {
                    Files.delete(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean viewDocumentAccepted(Document document, User user) {
        if (editDocumentAccepted(document, user)) return true;
        if (document.getViewLevel() == DocumentAccessLevel.ANYBODY) return true;
        if (document.getViewLevel() == DocumentAccessLevel.SOMEBODY && document.getViewUsersList().contains(user))
            return true;

        return false;
    }

    public boolean editDocumentAccepted(Document document, User user) {
        if (deleteDocumentAccepted(document, user)) return true;
        if (document.getEditLevel() == DocumentAccessLevel.ANYBODY) return true;
        if (document.getEditLevel() == DocumentAccessLevel.SOMEBODY && document.getEditUsersList().contains(user))
            return true;

        return false;
    }

    public boolean deleteDocumentAccepted(Document document, User user) {
        if (document.getOwner().getId() == user.getId()) return true;
        if (document.getDeleteLevel() == DocumentAccessLevel.ANYBODY) return true;
        if (document.getDeleteLevel() == DocumentAccessLevel.SOMEBODY && document.getDeleteUsersList().contains(user))
            return true;

        return false;
    }

}
