ALTER TABLE document
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE user
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
INSERT INTO user (confirmed, password, username) VALUES (1, 1, 1);