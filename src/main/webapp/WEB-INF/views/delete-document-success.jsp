<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false" %>
<html>
<head>
    <jsp:include page="components/head.jsp"/>
</head>
<body>
<jsp:include page="components/header.jsp"/>

<div class="container">
    <br/>
    <h1>Delete Success!</h1>
    <hr/>
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <p>Document <span class="font-weight-bold">${document.name}</span> was deleted!</p>
        </div>
    </div>
</div>
</body>
</html>