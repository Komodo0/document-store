<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="komodo.documentstore.model.entity.DocumentAccessLevel" %>
<%@ page import="komodo.documentstore.utils.HtmlUtils" %>
<%@ page session="false" %>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <jsp:include page="components/head.jsp"/>
</head>
<body>
<jsp:include page="components/header.jsp"/>

<div class="container">
    <br/>
    <h1>Document #${document.id}</h1>
    <hr/>
    <br/>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card">
                        <h5 class="card-header">Name:</h5>
                        <div class="card-body">
                            <p class="card-text">${document.name.concat(".").concat(document.extension)}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card">
                        <h5 class="card-header">Description:</h5>
                        <div class="card-body">
                            <p class="card-text" style="font-size: inherit; color: inherit; font-family: inherit">
                                ${HtmlUtils.getDescriptionForHtml(document.description)}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card">
                        <h5 class="card-header">Download:</h5>
                        <div class="card-body">
                            <p class="card-text"><a
                                    href="${pageContext.servletContext.contextPath}/doc/download/${document.id}">Click
                                for download</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card">
                        <h5 class="card-header">View access for:</h5>
                        <div class="card-body">
                            <ul class="card-text" id="viewAccessLevel">
                                <c:choose>
                                    <c:when test="${document.viewLevel == DocumentAccessLevel.SOMEBODY}">
                                        <c:choose>
                                            <c:when test="${document.viewUsersList.size() == 0}">
                                                <li>${DocumentAccessLevel.NOBODY}</li>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="user" items="${document.viewUsersList}">
                                                    <li>${user.username}</li>
                                                </c:forEach>
                                            </c:otherwise>

                                        </c:choose>

                                    </c:when>
                                    <c:otherwise>
                                        <li>${document.viewLevel}</li>
                                    </c:otherwise>
                                </c:choose>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card">
                        <h5 class="card-header">Edit access for:</h5>
                        <div class="card-body">
                            <ul class="card-text" id="editAccessLevel">
                                <c:choose>
                                    <c:when test="${document.editLevel == DocumentAccessLevel.SOMEBODY}">
                                        <c:choose>
                                            <c:when test="${document.editUsersList.size() == 0}">
                                                <li>${DocumentAccessLevel.NOBODY}</li>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="user" items="${document.editUsersList}">
                                                    <li>${user.username}</li>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <li>${document.editLevel}</li>
                                    </c:otherwise>
                                </c:choose>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card">
                        <h5 class="card-header">Delete access for:</h5>
                        <div class="card-body">
                            <ul class="card-text" id="deleteAccessLevel">
                                <c:choose>
                                    <c:when test="${document.deleteLevel == DocumentAccessLevel.SOMEBODY}">
                                        <c:choose>
                                            <c:when test="${document.deleteUsersList.size() == 0}">
                                                <li>${DocumentAccessLevel.NOBODY}</li>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="user" items="${document.deleteUsersList}">
                                                    <li>${user.username}</li>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <li>${document.deleteLevel}</li>
                                    </c:otherwise>
                                </c:choose>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group row text-right">
                <div class="col-sm-12">
                    <c:if test="${editAccepted}">
                        <a href="${pageContext.servletContext.contextPath}/doc/edit/${document.id}" type="button"
                           class="btn btn-primary">Edit document</a>
                    </c:if>
                    <c:if test="${deleteAccepted}">
                    <input name="submit" type="button" class="btn btn-danger" value="Delete" data-toggle="modal"
                           data-target="#submitDeleteModal"/>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" aria-labelledby="submitDeleteModal" tabindex="-1" id="submitDeleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Warning!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete the document <span class="font-weight-bold">${document.name}</span>?
                </p>
            </div>
            <div class="modal-footer">
                <a href="${pageContext.servletContext.contextPath}/doc/delete/${document.id}" type="button"
                   class="btn btn-danger">Delete document</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>