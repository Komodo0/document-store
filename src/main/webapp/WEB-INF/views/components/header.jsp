<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal"/>
<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar bg-dark">
    <a class="navbar-brand mr-0 mr-md-2" href="${pageContext.request.contextPath}/">Document Store</a>
    <div class="navbar-nav-scroll" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.servletContext.contextPath}/">Home</a>
            </li>
            <sec:authorize access="isAuthenticated()">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.servletContext.contextPath}/doc/list">Documents</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.servletContext.contextPath}/doc/add">Add document</a>
                </li>
            </sec:authorize>
        </ul>
    </div>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
        <sec:authorize access="isAuthenticated()">
            <li class="nav-item dropdown">
                <a class="nav-item nav-link dropdown-toggle mr-md-2" href="#" id="bd-versions" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="true">
                        ${user.username}
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/logout">Logout<span
                            class="sr-only">(current)</span></a>
                </div>
            </li>
        </sec:authorize>
        <sec:authorize access="!isAuthenticated()">
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/init">Registration<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/login">Login<span
                        class="sr-only">(current)</span></a>
            </li>
        </sec:authorize>
    </ul>
</header>
