<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="komodo.documentstore.model.entity.DocumentAccessLevel" %>
<%@ page session="false" %>
<%@ page language="java" contentType="text/html;charset=utf-8" %>
<html>
<head>
    <jsp:include page="components/head.jsp"/>
</head>
<body>
<jsp:include page="components/header.jsp"/>
<script>
    $(document).ready(function () {

        viewAccessLevel = $('#viewAccessLevel');
        editAccesssLevel = $('#editAccessLevel');
        deleteAccessLevel = $('#deleteAccessLevel');

        viewAccesssUsrIds = $("#viewAccessUserIds");
        editAccesssUsrIds = $("#editAccessUserIds");
        deleteAccesssUsrIds = $("#deleteAccessUserIds");

        if (viewAccessLevel.val() === "${DocumentAccessLevel.SOMEBODY}") {
            viewAccesssUsrIds.prop('disabled', false);
        } else {
            viewAccesssUsrIds.prop('disabled', true);
        }

        if (editAccesssLevel.val() === "${DocumentAccessLevel.SOMEBODY}") {
            editAccesssUsrIds.prop('disabled', false);
        } else {
            editAccesssUsrIds.prop('disabled', true);
        }

        if (deleteAccessLevel.val() === "${DocumentAccessLevel.SOMEBODY}") {
            deleteAccesssUsrIds.prop('disabled', false);
        } else {
            deleteAccesssUsrIds.prop('disabled', true);
        }

        viewAccessLevel.on('change', function () {
            if ($(this).val() === "${DocumentAccessLevel.SOMEBODY}") {
                viewAccesssUsrIds.prop('disabled', false);
            } else {
                viewAccesssUsrIds.prop('disabled', true);
            }
        });

        editAccesssLevel.on('change', function () {
            if ($(this).val() === "${DocumentAccessLevel.SOMEBODY}") {
                editAccesssUsrIds.prop('disabled', false);
            } else {
                editAccesssUsrIds.prop('disabled', true);
            }
        });

        deleteAccessLevel.on('change', function () {
            if ($(this).val() === "${DocumentAccessLevel.SOMEBODY}") {
                deleteAccesssUsrIds.prop('disabled', false);
            } else {
                deleteAccesssUsrIds.prop('disabled', true);
            }
        });
    });
</script>
<div class="container">
    <br/>
    <h1>Add document</h1>
    <hr/>
    <br/>
    <form:form method='POST' modelAttribute="documentCreationForm" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-6">
                <c:set var="nameErrors"><form:errors path="name"/></c:set>
                <c:set var="fileErrors"><form:errors path="file"/></c:set>
                <c:if test="${not empty nameErrors}">
                    <div class="alert alert-danger" role="alert">
                            ${nameErrors}
                    </div>
                </c:if>
                <c:if test="${not empty fileErrors}">
                    <div class="alert alert-danger" role="alert">
                            ${fileErrors}
                    </div>
                </c:if>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="" for="inlineFormInputGroupName">Name</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <input type='text' class="form-control" id="inlineFormInputGroupName"
                                   placeholder="Document name" name='name' value="${documentCreationForm.name}">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="" for="inlineFormInputGroupDescription">Description</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <textarea rows="5" class="form-control" id="inlineFormInputGroupDescription"
                                      placeholder="Document description"
                                      name='description'>${documentCreationForm.description}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="sr-only" for="inlineFormInputGroupFile">File</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <input type='file' class="" id="inlineFormInputGroupFile"
                                   placeholder="Put file here" name='file'/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label class="" for="viewAccessLevel">View access for:</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <select id="viewAccessLevel" name="viewAccessLevel" class="form-control">
                                <c:forEach var="item" items="${DocumentAccessLevel.values()}">
                                    <option
                                            <c:if test="${documentCreationForm.viewAccessLevel == item}">selected</c:if>
                                            value="${item}">${item}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="" for="viewAccessUserIds">Users</label>
                        <div class="input-group mb-2 mb-sm-0">

                            <select multiple id="viewAccessUserIds" name="viewAccessUserIds" class="form-control">
                                <c:forEach var="user" items="${usersList}">
                                    <option
                                            <c:if test="${documentCreationForm.viewAccessUserIds.contains(String.valueOf(user.id))}">selected</c:if>
                                            value="${user.id}">${user.username}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label class="" for="editAccessLevel">Edit access for:</label>
                        <div class="input-group mb-2 mb-sm-0">

                            <select id="editAccessLevel" name="editAccessLevel" class="form-control">
                                <c:forEach var="item" items="${DocumentAccessLevel.values()}">
                                    <option
                                            <c:if test="${documentCreationForm.editAccessLevel == item}">selected</c:if>
                                            value="${item}">${item}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="" for="editAccessUserIds">Users</label>
                        <div class="input-group mb-2 mb-sm-0">

                            <select multiple id="editAccessUserIds" name="editAccessUserIds" class="form-control">
                                <c:forEach var="user" items="${usersList}">
                                    <option
                                            <c:if test="${documentCreationForm.editAccessUserIds.contains(String.valueOf(user.id))}">selected</c:if>
                                            value="${user.id}">${user.username}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label class="" for="deleteAccessLevel">Delete access for:</label>
                        <div class="input-group mb-2 mb-sm-0">

                            <select id="deleteAccessLevel" name="deleteAccessLevel" class="form-control">
                                <c:forEach var="item" items="${DocumentAccessLevel.values()}">
                                    <option
                                            <c:if test="${documentCreationForm.deleteAccessLevel == item}">selected</c:if>
                                            value="${item}">${item}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="" for="deleteAccessUserIds">Users</label>
                        <div class="input-group mb-2 mb-sm-0">

                            <select multiple id="deleteAccessUserIds" name="deleteAccessUserIds" class="form-control">
                                <c:forEach var="user" items="${usersList}">
                                    <option
                                            <c:if test="${documentCreationForm.deleteAccessUserIds.contains(String.valueOf(user.id))}">selected</c:if>
                                            value="${user.id}">${user.username}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group row text-right">
                    <div class="col-sm-12">
                        <input name="submit" type="submit" class="btn btn-primary" value="Create"/>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>