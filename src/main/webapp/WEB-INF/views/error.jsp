<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="components/head.jsp"/>
</head>
<body>
<jsp:include page="components/header.jsp"/>

<div class="container">
    <br/>
    <h1>Error ${errorCode}</h1>
    <hr/>
    <br/>
    <div class="row">
        <div class="col-sm-12">
            ${errorText}
        </div>
    </div>
</div>
</body>
</html>