<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <jsp:include page="components/head.jsp"/>
    <link rel="stylesheet" type="text/css" href="DataTables-1.10.16/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="DataTables-1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript ">
        $(document).ready(function () {
            $('#docsTable').DataTable({
                "info": false,
                "lengthChange": false,
                "processing": true,
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ]
            });
        });
    </script>
</head>
<body>
<jsp:include page="components/header.jsp"/>

<div class="container">
    <br/>
    <h1>Available documents</h1>
    <hr/>
    <br/>
    <div class="row">
        <div class="col-12">
            <table class="table" id="docsTable">
                <thead>
                <tr>
                    <th>Name:</th>
                    <th>Creation date:</th>
                    <th>Owner:</th>
                    <th>Description:</th>
                    <th>Download:</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="document" items="${documents}">
                    <tr>
                        <td>
                            <a href="${pageContext.servletContext.contextPath}/doc/view/${document.id}">${document.name.concat(".").concat(document.extension)}</a>
                        </td>
                        <td>
                                ${document.uploadDate}
                        </td>
                        <td>
                                ${document.owner.username}
                        </td>
                        <td>
                                ${document.description}
                        </td>
                        <td>
                            <a href="${pageContext.servletContext.contextPath}/doc/download/${document.id}">Download</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>