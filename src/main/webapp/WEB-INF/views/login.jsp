<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="components/head.jsp"/>
</head>
<body>
<jsp:include page="components/header.jsp"/>
<div class="container">
    <br/>
    <h1>Login</h1>
    <hr/>
    <br/>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <c:if test="${not empty error}">
                <div class="alert alert-danger" role="alert">
                        ${error}
                </div>
            </c:if>
            <c:if test="${not empty msg}">
                <div class="alert alert-info" role="alert">
                        ${msg}
                </div>
            </c:if>
            <form name='loginForm' action="<c:url value='login' />" method='POST'>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="sr-only" for="inlineFormInputGroupUsername">Username</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon">@</div>
                            <input type='text' class="form-control" id="inlineFormInputGroupUsername"
                                   placeholder="Username" name='username'>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="sr-only" for="inlineFormInputGroupPassword">Password</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon">#</div>
                            <input type='password' class="form-control" id="inlineFormInputGroupPassword"
                                   placeholder="Password" name='password'/>
                        </div>
                    </div>
                </div>
                <div class="form-group row text-right">
                    <div class="col-sm-12">
                        <input name="submit" type="submit" class="btn btn-primary" value="Login"/>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </div>
        <div class="col-sm-4"></div>
    </div>
</div>
</body>
</html>
